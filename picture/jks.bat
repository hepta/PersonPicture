keytool -genkeypair ^
        -alias picture ^
        -keyalg RSA ^
        -keypass 123456 ^
        -sigalg SHA256withRSA ^
        -dname cn=picture,ou=picture,o=picture,l=tao,st=tao,c=CN ^
        -validity 3650 ^
        -keystore picture.jks ^
        -storetype JKS ^
        -storepass 123456