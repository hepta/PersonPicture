package com.tao.pictrue.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//{
//    "_id": "5e52a5ac8ad0cb82d197615a",
//    "author": "鸢媛",
//    "category": "Girl",
//    "createdAt": "2020-03-02 08:00:00",
//    "desc": "且听风吟，静待花开。",
//    "images": [
//    "http://gank.io/images/7fa98787d009465a9d196fbff6b0a5d7"
//    ],
//    "likeCounts": 1,
//    "publishedAt": "2020-03-02 08:00:00",
//    "stars": 1,
//    "title": "第12期",
//    "type": "Girl",
//    "url": "http://gank.io/images/7fa98787d009465a9d196fbff6b0a5d7",
//    "views": 558
//}
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private String title;
    private String publishedAt;
    private String desc;
    private Long views;
    private String url;
}
