package com.tao.data

// A generic class that contains data and status about loading this data.
sealed class Resource<T>(
    // 区分请求
    val which: Int = 0,
    // 数据结果
    val data: T? = null,
    // 错误信息
    val error: Pair<Int, String>? = null,
    // 下载百分比
    val percent: Float? = null
) {
    class Start<T>(which: Int = 0) : Resource<T>(which)
    class Success<T>(which: Int = 0, data: T) : Resource<T>(which, data)
    class Error<T>(which: Int = 0, error: Pair<Int, String>) : Resource<T>(which, error = error)
    class Progress<T>(which: Int = 0, percent: Float): Resource<T>(which, percent = percent)
    class Complete<T>(which: Int = 0) : Resource<T>(which)

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data = $data]"
            is Error -> "Error[exception = ${error?.first} ${error?.second}]"
            is Start<T> -> "Loading"
            is Complete -> "Complete"
            is Progress -> "Complete = $percent%"
        }
    }
}
