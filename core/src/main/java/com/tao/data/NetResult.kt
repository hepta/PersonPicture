package com.tao.data

/**
 * 服务端数据格式
 */
data class NetResult<T>(
    val status: Int = 0,
    val msg: String = "",
    val data: T?, // +? 防止空安全反序列化失败
    val count: Int = 0
)