package com.tao

import android.os.Bundle
import androidx.viewbinding.ViewBinding

/**
 * 基类，处理viewModel相关逻辑
 */
abstract class BaseActivity<VB : ViewBinding, VM : BaseViewModel> : BasicActivity<VB>() {
    protected abstract val viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        this.lifecycle.addObserver(viewModel)
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.lifecycle.removeObserver(viewModel)
    }
}