package com.tao.util

import android.content.res.Resources
import android.util.TypedValue

// px转dp
val Int.dp: Int
    get() {
        return toFloat().dp
    }

// px转dp
val Float.dp: Int
    get() {
        return (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            this,
            Resources.getSystem().displayMetrics
        ) + 0.5F).toInt()
    }