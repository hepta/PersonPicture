package com.tao

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.tao.net.ResponseSource
import com.tao.widget.TitleBar

/**
 * 干净的基类，不处理viewModel
 */
abstract class BasicFragment<VB : ViewBinding> : Fragment(), ResponseSource {

    protected abstract val binding: VB

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            initArgs(it)
        }
        initView()
        initObservable()
        initData()
    }

    /**
     * 初始化视图
     */
    protected open fun initView() {}

    /**
     * 初始化标题
     */
    protected fun setBackPressed(titleBar: TitleBar) {
        titleBar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
    }

    /**
     * 初始化bundle数据
     */
    protected open fun initArgs(bundle: Bundle) {}

    /**
     * 初始化数据
     * 发送请求
     */
    protected open fun initData() {}

    /**
     * 初始化Observable
     */
    protected open fun initObservable() {}
}