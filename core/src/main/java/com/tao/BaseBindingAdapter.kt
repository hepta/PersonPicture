package com.tao

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

/**
 * 包含Binding的适配器基类
 */
abstract class BaseBindingAdapter<T, VB : ViewBinding>(
    layoutResId: Int = -1
) :
    BaseQuickAdapter<T, BaseBindingAdapter.BaseBindingHolder<VB>>(layoutResId) {

    // 抽象方法 入参：(LayoutInflater, ViewGroup, Boolean)，出参：VB
    protected abstract val inflate: (LayoutInflater, ViewGroup, Boolean) -> VB

    // 创建ViewHolder
    override fun onCreateDefViewHolder(parent: ViewGroup, viewType: Int) =
        BaseBindingHolder(inflate(LayoutInflater.from(parent.context), parent, false))

    /**
     * ViewHolder
     */
    class BaseBindingHolder<VB : ViewBinding>(private val binding: VB) :
        BaseViewHolder(binding.root) {

        fun getViewBinding() = binding
    }
}

