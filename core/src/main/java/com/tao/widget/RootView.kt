package com.tao.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.tao.util.dp

/**
 * 根布局
 */
class RootView constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attributeSet, defStyleAttr) {
    private val titleBar: TitleBar = TitleBar(context)

    init {
        orientation = VERTICAL
        titleBar.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
        val height = 44.dp
        titleBar.minimumHeight = height
        // 添加title 高度44
        addView(titleBar, LayoutParams(LayoutParams.MATCH_PARENT, height))
    }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)

    /**
     * 获取标题
     */
    fun getTitleBar(): TitleBar {
        return titleBar
    }
}