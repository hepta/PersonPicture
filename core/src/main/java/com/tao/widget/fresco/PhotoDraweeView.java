package com.tao.widget.fresco;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.tao.widget.fresco.zoomable.DoubleTapGestureListener;
import com.tao.widget.fresco.zoomable.ZoomableDraweeView;

/**
 * PackageName : com.ziwenl.library.fresco
 * Author : Ziwen Lan
 * Date : 2020/5/21
 * Time : 16:46
 * Introduction :
 * 通过 Fresco Demo 自带的 ZoomableDraweeView 调整实现可缩放的 ImageView
 * <p>
 * 默认实现双击放大/缩小功能
 * <p>
 * 提供设置点击事件的回调监听
 * <p>
 * github 地址：https://github.com/ziwenL/ZoomableDraweeView
 */
public class PhotoDraweeView extends ZoomableDraweeView {

    private DoubleTapGestureListener mDoubleTapGestureListener;

    public PhotoDraweeView(Context context) {
        super(context);
        init();
    }

    public PhotoDraweeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PhotoDraweeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = measureSize(widthMeasureSpec);
        int height = measureSize(heightMeasureSpec);
        if (width > 0 && height > 0) {
            setMeasuredDimension(width, height);
        }
    }

    private int measureSize(int spec) {
        int result = 0; //结果
        int specMode = MeasureSpec.getMode(spec);
        int specSize = MeasureSpec.getSize(spec);
        switch (specMode) {
            case MeasureSpec.AT_MOST:  // 子容器可以是声明大小内的任意大小
//                XLog.e("子容器可以是声明大小内的任意大小");
//                XLog.e("大小为:" + specSize);
                result = specSize;
                break;
            case MeasureSpec.EXACTLY: //父容器已经为子容器设置了尺寸,子容器应当服从这些边界,不论子容器想要多大的空间.  比如EditTextView中的DrawLeft
//                XLog.e("父容器已经为子容器设置了尺寸,子容器应当服从这些边界,不论子容器想要多大的空间");
//                XLog.e("大小为:" + specSize);
                result = specSize;
                break;
//            case MeasureSpec.UNSPECIFIED:  //父容器对于子容器没有任何限制,子容器想要多大就多大. 所以完全取决于子view的大小
//                XLog.e("父容器对于子容器没有任何限制,子容器想要多大就多大");
//                XLog.e("大小为:" + specSize);
//                result = 1500;
//                break;
            default:
                break;
        }
        return result;
    }

    private void init() {
        //实现双击放大/缩小功能
        mDoubleTapGestureListener = new DoubleTapGestureListener(this);
        setTapListener(mDoubleTapGestureListener);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        ViewGroup.LayoutParams lp = getLayoutParams();
        lp.width = w;
        lp.height = h;
        setLayoutParams(lp);
    }

    /**
     * 资源转 uri
     */
    private Uri imageTranslateUri(@DrawableRes int resId) {
        Resources r = getContext().getResources();
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
                + r.getResourcePackageName(resId) + "/"
                + r.getResourceTypeName(resId) + "/"
                + r.getResourceEntryName(resId));
    }

    /**
     * 填充显示资源图片
     */
    public void setImageRes(@DrawableRes int resId) {
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(imageTranslateUri(resId))
                .build();
        setControllers(controller, null);
    }

    /**
     * 填充显示网络图片
     */
    public void setImageUrl(String imageUrl) {
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(imageUrl)
                .build();
        setControllers(controller, null);
    }

    /**
     * 重写覆盖实现点击事件的回调监听
     */
    @Override
    public void setOnClickListener(@Nullable final View.OnClickListener onClickListener) {
        DoubleTapGestureListener.OnSingleClickListener onSingleClickListener =
                onClickListener == null ? null : () -> onClickListener.onClick(PhotoDraweeView.this);
        mDoubleTapGestureListener.setOnSingleClick(onSingleClickListener);
        this.setTapListener(mDoubleTapGestureListener);
    }
}
