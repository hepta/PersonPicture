package com.tao

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.tao.net.ResponseSource
import com.tao.widget.TitleBar

/**
 * 干净的基类，不处理viewModel
 */
abstract class BasicActivity<VB : ViewBinding> : AppCompatActivity(), ResponseSource {

    protected abstract val binding: VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initView()
        initObservable()
        initData()
    }

    /**
     * 初始化视图
     */
    protected open fun initView() {}

    /**
     * 初始化标题
     */
    protected fun setBackPressed(titleBar: TitleBar) {
        titleBar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    /**
     * 初始化数据
     * 发送请求
     */
    protected open fun initData() {}

    /**
     * 初始化Observable
     */
    protected open fun initObservable() {}
}