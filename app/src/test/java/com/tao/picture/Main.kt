package com.tao.picture

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tao.picture.ui.person.data.entity.PersonList

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        println(1.6.toInt())
    }

    fun json() {
        val json = "{\"counts\":0,\"msg\":\"\\u51fa\\u9519\\u4e86~\",\"status\":101}"
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
        val adapter = moshi.adapter(PersonList::class.java)
        val fromJson = adapter.fromJson(json)
        println(fromJson)
    }

    fun fetchName() {
        val url = "http://gank.io/images/7fa98787d009465a9d196fbff6b0a5d7/"
        var saveName = ".jpg"
        if (saveName.isEmpty() || saveName.startsWith(".")) {
            var link = url
            if (link.endsWith("/")) {
                // 移除末尾的"/"
                link = link.removeSuffix("/")
            }
            if (url.contains("/")) {
                //文件原名称
                val tempName = link.substring(link.lastIndexOf("/") + 1)
                // 有可能是单纯的没有后缀
                if (!tempName.contains(".")) {
                    // 拼接传入的后缀
                    saveName = tempName + saveName
                }
            }
        }
        println(saveName)
        var suffix = saveName.substring(saveName.lastIndexOf("."))
        println(suffix)
    }
}