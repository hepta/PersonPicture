package com.tao.picture.base.db

import androidx.room.TypeConverter
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Types
import com.tao.di.getMoshi

/**
 * List<String>转Json
 */
class StringListConverter {

    private val adapter : JsonAdapter<List<String>> by lazy {
        val moshi = getMoshi()
        // 创建type 等同List<String>
        val type = Types.newParameterizedType(
            List::class.java,
            String::class.java
        )
        moshi.adapter(type)
    }



    @TypeConverter
    fun getListFromString(value: String): List<String> {
        // 反序列化
        return adapter.fromJson(value) as List<String>
    }

    @TypeConverter
    fun saveListToString(list: List<String>): String {
        // 序列化
        return adapter.toJson(list)
    }
}