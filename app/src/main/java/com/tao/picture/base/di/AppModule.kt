package com.tao.picture.base.di

import android.content.Context
import com.tao.picture.base.data.PreferenceStore
import com.tao.picture.base.db.ImageDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


/**
 * 全局的依赖模块
 * InstallIn安装入SingletonComponent组件
 * SingletonComponent作用于全局
 * 通过Provides创建对象可以不用@Inject
 */
@Module // 在提供依赖项的类(即创建对象的类)上方使用了此注释
@InstallIn(SingletonComponent::class)
class AppModule {

    /**
     * 数据库实例
     * @ApplicationContext 是一种@Qualifier修饰器实现，用于区分Context和ApplicationContext
     */
    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): ImageDatabase =
        ImageDatabase.getDatabase(context)


    /**
     * Preference实例
     */
    @Singleton
    @Provides
    fun providePreferenceStore(@ApplicationContext context: Context): PreferenceStore =
        PreferenceStore(context)
}