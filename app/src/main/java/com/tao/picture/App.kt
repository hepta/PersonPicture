package com.tao.picture

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDexApplication
import com.elvishew.xlog.LogConfiguration
import com.elvishew.xlog.LogLevel
import com.elvishew.xlog.XLog
import com.elvishew.xlog.interceptor.BlacklistTagsFilterInterceptor
import com.facebook.drawee.backends.pipeline.Fresco
import dagger.hilt.android.HiltAndroidApp

/**
 * application依赖
 * @HiltAndroidApp 会触发 Hilt 的代码生成操作，生成的代码包括应用的一个基类，
 * 该基类充当应用级依赖项容器。
 *
 * 整体设计
 * https://blog.csdn.net/tonydz0523/article/details/108562021
 * https://github.com/ahmedeltaher/MVVM-Kotlin-Android-Architecture
 * https://developer.android.google.cn/guide?hl=zh_cn
 * https://github.com/zhouzikk/KotlinRetrofit
 * https://www.jianshu.com/p/06bb5ced9e61
 *
 * moshi
 * https://blog.csdn.net/qq_20330595/article/details/89205377
 * http://www.chenhe.cc/p/339
 *
 * hilt
 * https://dagger.dev/hilt/
 * https://blog.csdn.net/www1575066083/article/details/118092326 hilt依赖
 * https://www.jianshu.com/p/96eb1bed7861
 * https://juejin.cn/post/6967148539277213733
 * 多module直接使用，dfm需要特殊处理
 * https://github.com/riodext/AndroidDaggerHiltMVP
 * https://developer.android.google.cn/training/dependency-injection/hilt-multi-module
 *
 * adapter
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 * https://www.jianshu.com/p/1a95e8af5efb
 * https://github.com/DylanCaiCoding/ViewBindingKTX
 *
 * room
 * https://www.jianshu.com/p/4ac12bc56539
 * https://blog.csdn.net/tao_zi7890/article/details/100076975
 *
 * 下载
 * https://github.com/Petterpx/LiveHttp
 * https://github.com/JiangHaiYang01/RxHttp
 * https://github.com/ssseasonnn/DownloadX
 *
 * navigation
 * https://www.cnblogs.com/changyiqiang/p/14927567.html
 *
 * toolbar
 * https://blog.csdn.net/jungle_pig/article/details/52785781
 *
 * livedata
 * https://developer.android.google.cn/codelabs/advanced-kotlin-coroutines#0
 * https://blog.csdn.net/u011897062/article/details/112272235
 * https://www.zhihu.com/question/335776098/answer/755242861
 */
@HiltAndroidApp
class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        context = this
        // 现在很多项目都用glide，建议用glide
        // 初始化fresco
        Fresco.initialize(this);
        // 初始化XLog
        val build = LogConfiguration.Builder()
            .logLevel(LogLevel.ALL)
            .tag("T800")
        // release不显示
        if (!BuildConfig.DEBUG) {
            build.addInterceptor(
                BlacklistTagsFilterInterceptor(    // 添加黑名单 TAG 过滤器
                    "T800"
                )
            )
        }
        XLog.init(build.build())
    }

    companion object {
        // 改用ContextProvider类提供的context
        private lateinit var context: Application
        fun getContext(): Context {
            return context
        }
    }
}