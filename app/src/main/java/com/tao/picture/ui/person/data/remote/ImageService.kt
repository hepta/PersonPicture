package com.tao.picture.ui.person.data.remote

import com.tao.picture.ui.person.data.entity.Person
import dagger.hilt.android.scopes.ViewModelScoped
import retrofit2.http.GET

/**
 * retrofit的service
 */
@ViewModelScoped
interface ImageService {

//    @GET("random/category/{category}/type/{type}/count/{count}")
//    suspend fun getInfo(
//        @Path("category") category: String,
//        @Path("type") type: String,
//        @Path("count") count: Int
//    ): List<Girl>


    @GET("pic/10")
    suspend fun getImages(): List<Person>

    @GET("pic/1")
    suspend fun getImage(): List<Person>

}