package com.tao.picture.ui.person.di

import com.tao.picture.ui.person.data.entity.User
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped

@Module
@InstallIn(ActivityComponent::class)
class TestModule {

    @ActivityScoped
    @Provides
    fun provideUser() = User("张三", 8)
}