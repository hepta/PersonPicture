package com.tao.picture.ui.person.activity

import android.content.Context
import android.content.Intent
import androidx.core.os.bundleOf
import androidx.fragment.app.commit
import com.tao.BasicActivity
import com.tao.picture.R
import com.tao.picture.databinding.ActivityDetailBinding
import com.tao.picture.ui.person.fragment.DetailFragment
import dagger.hilt.android.AndroidEntryPoint

/**
 * @AndroidEntryPoint 表示注解入口，用到hilt的组件都要加
 */
@AndroidEntryPoint
class DetailActivity : BasicActivity<ActivityDetailBinding>() {

    override val binding: ActivityDetailBinding by lazy {
        ActivityDetailBinding.inflate(layoutInflater)
    }

    override fun initData() {
        super.initData()
        // 创建fragment 使用fragment-ktx库创建
        // https://developer.android.google.cn/kotlin/ktx 官方ktx讲解
        supportFragmentManager.commit {
//            addToBackStack("...")
//            setCustomAnimations(
//                R.anim.enter_anim,
//                R.anim.exit_anim)
//            add(fragment, "...")
            val url = intent.getStringExtra(KEY_URL) ?: ""
            val fragment = DetailFragment()
            fragment.arguments = bundleOf(KEY_URL to url)
            replace(R.id.root, fragment)
        }
    }

    companion object {
        const val KEY_URL = "url"
        // 页面入口
        fun launch(context: Context, url: String) {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(this.KEY_URL, url)
            context.startActivity(intent)
        }
    }
}