package com.tao.picture.ui.person.data.repository

import com.tao.picture.ui.person.data.entity.Person
import com.tao.picture.ui.person.data.local.ImageDao
import com.tao.picture.ui.person.data.remote.ImageRemoteData
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlin.coroutines.CoroutineContext

@ViewModelScoped
class ImageRepository constructor(
    private val remoteData: ImageRemoteData,
    private val localData: ImageDao,
    private val ioDispatcher: CoroutineContext
) {
    /**
     * 取一张图片，取列表的第一张
     * id没有用到
     */
    fun getImage(id: String) = flow {
        emit(remoteData.getImage())
    }.flowOn(ioDispatcher) // 指定网络请求的线程

    /**
     * 取网络图片
     */
    fun getImages() = flow {
        emit(remoteData.getImages())
    }.flowOn(ioDispatcher)

    suspend fun getImages2() = remoteData.getImages();

    /**
     * 数据存本地
     */
    fun saveImages(people: List<Person>) = flow {
        emit(localData.insert(people))
    }.flowOn(ioDispatcher)

    /**
     * 数据存本地
     */
    fun saveImage(person: Person) = flow {
        emit(localData.insert(person))
    }.flowOn(ioDispatcher)
}