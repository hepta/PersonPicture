package com.tao.picture.ui.person.data.entity

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PersonList(
    val counts: Int = 0,
    val data: List<Person> = listOf(),
    val status: Int = 0
)