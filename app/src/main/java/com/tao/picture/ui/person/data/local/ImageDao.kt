package com.tao.picture.ui.person.data.local

import androidx.room.*
import com.tao.picture.ui.person.data.entity.Person

@Dao
interface ImageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(images: List<Person>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(image: Person)

    /**
     * 返回更新成功的个数
     */
    @Update
    suspend fun update(images: List<Person>): Int

    @Update
    suspend fun update(image: Person): Int

    @Delete
    suspend fun delete(images: List<Person>)

    @Delete
    suspend fun delete(image: Person)

    @Query("DELETE FROM person")
    suspend fun clean()

    @Query("SELECT * FROM person ORDER BY views DESC")
    suspend fun getImage() : List<Person>

    @Query("SELECT * FROM person WHERE id = :id")
    suspend fun getImage(id: String): Person

//    @Query("SELECT * FROM person WHERE id = :id")
//    fun getImage(id: String): LiveData<Person>
//
//    @Query("SELECT * FROM person WHERE id = :id")
//    suspend fun getImage(id: String): Flow<Person>
}