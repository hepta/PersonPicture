package com.tao.picture.ui.person.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.tao.BaseBindingAdapter
import com.tao.picture.R
import com.tao.picture.databinding.ItemPersonBinding
import com.tao.picture.ui.person.data.entity.Person

/**
 * Person列表的适配器
 */
class PersonListAdapter(
    layoutResId: Int = R.layout.item_person
) : BaseBindingAdapter<Person, ItemPersonBinding>(layoutResId) {

    override val inflate: (LayoutInflater, ViewGroup, Boolean) -> ItemPersonBinding
        get() = { inflater, viewGroup, attachToRoot ->
            ItemPersonBinding.inflate(inflater, viewGroup, attachToRoot)
        }

    override fun convert(holder: BaseBindingHolder<ItemPersonBinding>, item: Person) {
        holder.getViewBinding()
            .apply {
                name.text = "${item.title} ${item.publishedAt}"
                speciesAndStatus.text = "${item.desc}\n\n${item.views}人查看"
                image.setImageURI(item.url)
            }
    }

}