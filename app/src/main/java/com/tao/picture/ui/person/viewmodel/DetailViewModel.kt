package com.tao.picture.ui.person.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.tao.BaseViewModel
import com.tao.picture.ui.person.data.entity.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    application: Application
) : BaseViewModel(application) {

    // 改变id来改变user
    val id = MutableLiveData<Int>()

    // CoroutineLiveData类中有示例代码
    val user: LiveData<User> = Transformations.switchMap(id) {
        if (it == 1) {
            liveData {
                /* 生产时使用挂起函数操作 */
                delay(1000)
                emit(User("张三", 8))
            }
        } else {
            liveData {
                /* 生产时使用挂起函数操作 */
                delay(1000)
                emit(User("李四", 9))
            }
        }
    }
}