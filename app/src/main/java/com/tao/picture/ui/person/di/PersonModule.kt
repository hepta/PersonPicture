package com.tao.picture.ui.person.di

import com.tao.net.ServiceGenerator
import com.tao.picture.base.db.ImageDatabase
import com.tao.picture.ui.person.data.local.ImageDao
import com.tao.picture.ui.person.data.remote.ImageRemoteData
import com.tao.picture.ui.person.data.remote.ImageService
import com.tao.picture.ui.person.data.repository.ImageRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlin.coroutines.CoroutineContext

/**
 * ViewModelComponent和ViewModelScoped对应
 */
@Module
@InstallIn(ViewModelComponent::class)
class PersonModule {

    /**
     * service实例
     */
    @ViewModelScoped
    @Provides
    fun provideService(generator: ServiceGenerator) = generator.create(ImageService::class.java)

    /**
     * ImageRemoteData实例，提供远程数据
     */
    @ViewModelScoped
    @Provides
    fun provideRemoteData(imageService: ImageService) = ImageRemoteData(imageService)

    /**
     * ImageRepository包括了远程以及本来数据的操作，通过他来和viewModel对接
     */
    @ViewModelScoped
    @Provides
    fun provideRepository(remoteData: ImageRemoteData, localData: ImageDao, io: CoroutineContext) =
        ImageRepository(remoteData, localData, io)

    /**
     * ImageDao实例，获取本地数据
     */
    @ViewModelScoped
    @Provides
    fun provideImageDao(db: ImageDatabase): ImageDao = db.imageDao
}