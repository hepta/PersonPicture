package com.tao.picture.ui.person.fragment

import androidx.fragment.app.viewModels
import com.elvishew.xlog.XLog
import com.tao.BaseFragment
import com.tao.net.addObserve
import com.tao.picture.databinding.FragmentPersonBinding
import com.tao.picture.ui.person.activity.DetailActivity
import com.tao.picture.ui.person.adapter.PersonListAdapter
import com.tao.picture.ui.person.data.entity.Person
import com.tao.picture.ui.person.viewmodel.PersonViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PersonFragment : BaseFragment<FragmentPersonBinding, PersonViewModel>() {

    override val viewModel: PersonViewModel by viewModels()

    private lateinit var adapter: PersonListAdapter

    override val binding: FragmentPersonBinding by lazy {
        FragmentPersonBinding.inflate(layoutInflater)
    }

    override fun initObservable() {
        super.initObservable()
        /**
         * 添加监听
         */
        addObserve(viewModel.person) {
            // drop丢弃前n条数据 List的drop函数可以丢弃数据
//            viewModel.saveImage(it.data.get(0)) // 可以测试db的保存功能
            // 显示数据
            adapter.addData(it)
        }

        // 监听下载
        addObserve(viewModel.img, {
            XLog.e(it)
        }) {
            XLog.e(it)
        }
    }

    override fun initData() {
        super.initData()
        // 初始化适配器
        adapter = PersonListAdapter()
        // 监听点击事件
        adapter.setOnItemClickListener { adapter, view, position ->
            val item = adapter.getItem(position) as Person
            // 跳转详情页
            context?.run {
                DetailActivity.launch(this, item.url)
            }
        }
        // 设置适配器
        binding.recyclerView.adapter = adapter
        // 请求数据
        viewModel.getImage()
        // 下载
//        viewModel.download()
//        lifecycleScope.launch {
//            delay(5000)
//            viewModel.getImage()
//        }
        // 一些测试
//        lifecycleScope.launch {
//            withContext(Dispatchers.IO) {
//            }
//            repeat(1000) {
//                XLog.e("计时：$it")
//                delay(1000)
//            }
//        }
    }

}