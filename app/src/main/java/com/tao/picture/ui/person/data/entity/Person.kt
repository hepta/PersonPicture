package com.tao.picture.ui.person.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.tao.picture.base.db.StringListConverter

//{
//    "_id": "5e52a5ac8ad0cb82d197615a",
//    "author": "鸢媛",
//    "category": "Girl",
//    "createdAt": "2020-03-02 08:00:00",
//    "desc": "且听风吟，静待花开。",
//    "images": [
//    "http://gank.io/images/7fa98787d009465a9d196fbff6b0a5d7"
//    ],
//    "likeCounts": 1,
//    "publishedAt": "2020-03-02 08:00:00",
//    "stars": 1,
//    "title": "第12期",
//    "type": "Girl",
//    "url": "http://gank.io/images/7fa98787d009465a9d196fbff6b0a5d7",
//    "views": 558
//}
@JsonClass(generateAdapter = true)
@Entity(tableName = "person")
@TypeConverters(StringListConverter::class)
data class Person(
    // json别名
    @Json(name = "_id")
    // 主键
    @PrimaryKey
    var id: String = "",
    var author: String = "",
    var category: String = "",
    // 数据库别名
    @ColumnInfo(name = "created_at")
    var createdAt: String = "",
    var desc: String = "",
    // 忽略，使用Ignore并不能忽略List<String>
//    @Ignore
    var images: List<String>?,
    @ColumnInfo(name = "like_counts")
    var likeCounts: Long = 0L,
    @ColumnInfo(name = "published_at")
    var publishedAt: String = "",
    var stars: Long = 0L,
    var title: String = "",
    var type: String = "",
    var url: String = "",
    var views: Long = 0L,
)