package com.tao.picture.ui.person.data.remote

import dagger.hilt.android.scopes.ViewModelScoped

/***
 * 我个人理解：作用域或者是生命周期跟随ViewModel
 */
@ViewModelScoped
class ImageRemoteData constructor(
    private val imageService: ImageService,
) {

    /**
     * 获取多张图片
     */
    suspend fun getImages() = imageService.getImages()

    /**
     * 获取一张图片
     */
    suspend fun getImage() = imageService.getImage()

//    suspend fun getInfo(
//        category: String,
//        type: String,
//        count: Int,
//    ) = imageService.getInfo(category, type, count)
}