package com.tao.picture.ui.person.fragment

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.view.menu.MenuBuilder
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import com.elvishew.xlog.XLog
import com.tao.BaseFragment
import com.tao.data.Resource
import com.tao.net.addObserve
import com.tao.net.download.download
import com.tao.picture.R
import com.tao.picture.databinding.FragmentDetailBinding
import com.tao.picture.ui.person.activity.DetailActivity
import com.tao.picture.ui.person.viewmodel.DetailViewModel
import com.tao.util.ToastUtil
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : BaseFragment<FragmentDetailBinding, DetailViewModel>() {

    override val viewModel: DetailViewModel by viewModels()

    private var url = ""
    val img: MutableLiveData<Resource<Uri>> = MutableLiveData()

    override val binding: FragmentDetailBinding by lazy {
        FragmentDetailBinding.inflate(layoutInflater)
    }

    @SuppressLint("RestrictedApi")
    override fun initView() {
        super.initView()
        binding.title.apply {
            // 设置返回监听
            setBackPressed(this)
            // 更多按钮
            overflowIcon = ContextCompat.getDrawable(context, R.drawable.more)
            // 注入menu
            inflateMenu(R.menu.save, true)
            // 设置溢出菜单展示图标
            (menu as MenuBuilder).setOptionalIconsVisible(true)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_save -> {
                        // 下载图片
                        download()
                    }
                    else -> {
                        // 改变id的值，触发user执行协程里的函数
                        viewModel.id.value?.let {
                            if (it == 1) {
                                viewModel.id.value = 0
                            } else {
                                viewModel.id.value = 1
                            }
                        }
                        // 没有值，赋初值1
                        viewModel.id.value ?: run {
                            viewModel.id.value = 1
                        }
                    }
                }
                true
            }
//            showOverflowMenu() // 展示菜单弹窗
//            dismissPopupMenus() // 隐藏菜单弹窗
        }
    }

    override fun initArgs(bundle: Bundle) {
        super.initArgs(bundle)
        // 获取url
        url = bundle.getString(DetailActivity.KEY_URL, "") ?: ""
        XLog.e(url)
    }

    override fun initObservable() {
        super.initObservable()
        // 添加下载监听
        addObserve(img) {
            XLog.e(it)
            ToastUtil.longToast("下载成功")
        }
        // 监听user改变
        viewModel.user.observe(this) {
            XLog.e(it)
            ToastUtil.toast(it.name)
        }
    }

    override fun initData() {
        super.initData()
        // 展示图片
        if (url.isEmpty()) {
            binding.image.setImageUrl("https://www.helloimg.com/images/2020/05/18/splash_bgb5d0ea1e3c8a4cc8.jpg")
        } else {
            binding.image.setImageUrl(url)
        }
//        binding.image.setImageUrl("https://tpc.googlesyndication.com/simgad/8662816701681352467?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0DwkwlYAWBfcAKAAQGIAQGdAQAAgD-oAQGwAYCt4gS4AV_FAS2ynT4&rs=AOga4qlMruPOMpPatum_mJgXIFUKw6LbdQ")
    }

    /**
     * 下载函数
     */
    fun download() {
        download("https://www.helloimg.com/images/2020/05/18/splash_bgb5d0ea1e3c8a4cc8.jpg", img)
    }
}